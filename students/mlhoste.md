# Mickaël LHOSTE
![Mickaël LHOSTE](../images/mlhoste.png)

Mickaël LHOSTE (en polonais : Geralt z Rivii) est un personnage de fiction, protagoniste principal de la série de romans et nouvelles Le Sorceleur (Wiedźmin) de l’auteur polonais Andrzej Sapkowski.